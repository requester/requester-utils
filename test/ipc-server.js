import { ServerSide } from "../ipc.js";
import Path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs';

const __dirname = Path.dirname(fileURLToPath(import.meta.url));

const server = new ServerSide();
server.startup().then(() => {
    server.observe("test", ({ params }) => {
        console.log(params);
        return {
            data: { bb: "bb" },
            body: fs.createReadStream(Path.resolve(__dirname, './fs.js'))
        };
    });
})