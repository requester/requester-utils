import { isObject } from "./is.js";

export function walkJson(data, parent, key, fn) {
    let r = fn(data, parent, key);
    if (!r) {
        return;
    }
    if (Array.isArray(data)) {
        data.forEach((a, index) => {
            walkJson(a, data, index, fn);
        });
    } else if (isObject(data)) {
        Reflect.ownKeys(data).forEach(key => {
            walkJson(data[key], data, key, fn);
        });
    }
}