import { md5 } from "hash-wasm";

export async function getmd5(string) {
    return md5(string);
}

export function hashCode(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        var character = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + character;
        hash = hash & hash;
    }
    return hash;
}

export function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export function randomid(len = 7) {
    if (len <= 2) {
        len = 7;
    }
    return Math.random().toString(36).slice(2, len + 2);
}