import { ClientSide } from "../ipc.js";
import Path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs';

const __dirname = Path.dirname(fileURLToPath(import.meta.url));

const client = new ClientSide();
client.connect().then(() => {
    client.publish("test", {
        params: { aa: "aa" }
    }).then(({ data, body }) => {
        console.log(data);
        body.pipe(fs.createWriteStream(Path.resolve(__dirname, './fs2.js')));
    }).catch(e=>{
        console.log(e);
    });
}).catch(e=>console.log(e));