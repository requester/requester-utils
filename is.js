export function isString(obj) {
	return (typeof obj === 'string') && obj.constructor === String;
}

export function isFunction(obj) {
	return (typeof obj === 'function') && obj.constructor === Function;
}

export function isObject(obj) {
	return typeof (obj) === "object" && Object.prototype.toString.call(obj).toLowerCase() === "[object object]" && !obj.length;
}

export function isPlainObject(obj) {
	return isObject(obj) && obj.constructor.prototype === Object.prototype;
}

export function isArray(obj) {
	return Array.isArray(obj);
}

export function isNumeric(obj) {
    return !isNaN(parseFloat(obj)) && isFinite(obj);
}

export function isClass(func) {
    return typeof func === 'function' && /^class\s/.test(Function.prototype.toString.call(func));
}