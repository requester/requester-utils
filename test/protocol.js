import { MessageObserver } from "./../protocol.js";
import Path from 'path';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { PassThrough } from "stream";

const __dirname = Path.dirname(fileURLToPath(import.meta.url));

const pass = new PassThrough();
const pass2 = new PassThrough();
// DataSender.sentTo(pass, {
//     body: fs.createReadStream(Path.resolve(__dirname, './fs.js')),
//     params: { aa: "aa" }
// });
// DataReceiver.from(pass).on('request', request => {
//     request.body.pipe(fs.createWriteStream(Path.resolve(__dirname, './fs2.js')));
// });

const observer = MessageObserver.form({
    readable: pass2,
    writable: pass
});
observer.publish('test', {
    aa: "aa",
    file: fs.createReadStream(Path.resolve(__dirname, './fs.js'))
}).then(params => {
    console.log('+++++++');
    // console.log(params.file);
    params.file.pipe(fs.createWriteStream(Path.resolve(__dirname, './fs2.js')));
});

const observer2 = MessageObserver.form({
    readable: pass,
    writable: pass2
});
observer2.subscribe('test', params => {
    // console.log(params.file);
    return params;
});